'''
    Contributors: Dezhao Wang, Yueyu Hu, Ding Ding, Wei Wang, Tong Chen, Chuanmin Jia, Yihang Chen
'''

import math
import numpy as np
import torch
import torch.nn as nn
import Util.torch_msssim as torch_msssim
from Util.config import dict

USE_MULTI_HYPER = dict["USE_MULTI_HYPER"]
USE_PREDICTOR = dict["USE_PREDICTOR"]


class SubstituteGenerator(object):
    def __init__(self, model, context_model, llambda=4, num_steps=20, step_size=0.0001, reconstruct_metric='mse'):
        self.model = model
        self.context_model = context_model
        self.llambda = llambda
        self.reconstruct_metric = reconstruct_metric
        self.step_size = step_size
        self.num_steps = num_steps
        self.msssim_func = torch_msssim.MS_SSIM(max_val=1.).cuda()

    def get_loss(self, reconstructed_image, xp1, xp2, xp3, orig_image):
        N, _, H, W = orig_image.size()
        out = {}
        num_pixels = N * H * W

        train_bpp2 = torch.sum(torch.log(xp2)) / (-np.log(2) * num_pixels)
        train_bpp3 = torch.sum(torch.log(xp3)) / (-np.log(2) * num_pixels)
        out['bpp_loss'] = train_bpp2 + train_bpp3

        if USE_MULTI_HYPER:
            train_bpp1 = torch.sum(torch.log(xp1)) / (-np.log(2) * num_pixels)
            out['bpp_loss'] = out['bpp_loss'] + train_bpp1

        if self.reconstruct_metric == 'mse':
            out['distortion_loss'] = nn.MSELoss()(reconstructed_image, orig_image)
        elif self.reconstruct_metric == 'msssim':
            out['distortion_loss'] = (1 - self.msssim_func(reconstructed_image, orig_image)).cuda()
        else:
            raise ValueError("metric %s is not supported. Use mse or msssim.")
        out['overall_loss'] = self.llambda * out['distortion_loss'] + out['bpp_loss']

        return out

    #
    def perturb(self, orig_image, orig_rd=None, lambda_rd=None):
        # assert orig_image shape
        assert len(orig_image.shape) == 4, "orig_image must have 4 dimensions (1, None, None, 3)"
        orig_image_copy = orig_image.clone()
        if orig_rd:
            inital_loss = orig_rd
        else:
            with torch.no_grad():
                if USE_MULTI_HYPER:
                    if USE_PREDICTOR:
                        output, xp1_, xp2, xq1_res, xq1, hyper_dec, xp3, xq3 = self.model(orig_image, 2)
                        xp1, _ = self.context_model(xq1_res, xq1, hyper_dec)
                    else:
                        output, xp1_, xp2, xq1, hyper_dec, xp3, xq3 = self.model(orig_image, 2)
                        xp1, _ = self.context_model(xq1, hyper_dec)
                else:
                    if lambda_rd:
                        output, xp1, xp2, xq1, x3 = self.model(orig_image, 2, lambda_rd)
                    else:
                        output, xp1, xp2, xq1, x3 = self.model(orig_image, 2)
                    xp3, _ = self.context_model(xq1, x3)
                output.clamp_(0, 1)
                out_criterion = self.get_loss(output, xp1, xp2, xp3, orig_image)
            inital_loss = out_criterion['overall_loss'].detach().item()
            # print("inital loss is: " + str(inital_loss))
            if USE_MULTI_HYPER:
                del output, xp1_, xp2, xq1, hyper_dec, xp3, xq3, xp1, out_criterion
                if USE_PREDICTOR:
                    del xq1_res
            else:
                del output, xp1, xp2, xq1, x3, xp3, out_criterion

        anchor_image = orig_image.data.clone()

        # main training loop
        for i in range(self.num_steps):
            # find a substitute (for one step)
            orig_image.requires_grad = True
            if USE_MULTI_HYPER:
                if USE_PREDICTOR:
                    output, xp1_, xp2, xq1_res, xq1, hyper_dec, xp3, xq3 = self.model(orig_image, 1)
                    xp1, _ = self.context_model(xq1_res, xq1, hyper_dec)
                else:
                    output, xp1_, xp2, xq1, hyper_dec, xp3, xq3 = self.model(orig_image, 1)
                    xp1, _ = self.context_model(xq1, hyper_dec)
            else:
                if lambda_rd:
                    output, xp1, xp2, xq1, x3 = self.model(orig_image, 1, lambda_rd)
                else:
                    output, xp1, xp2, xq1, x3 = self.model(orig_image, 1)
                xp3, _ = self.context_model(xq1, x3)

            output.clamp_(0, 1)
            out_criterion = self.get_loss(output, xp1, xp2, xp3, anchor_image)
            out_criterion['overall_loss'].backward()
            grad1 = orig_image.grad
            changes = grad1 * self.step_size
            orig_image = orig_image - changes
            orig_image = torch.clamp(orig_image, min=0, max=1).detach_()

        # check the loss of updated substitute image, keep the one if it has lower loss
        with torch.no_grad():
            if USE_MULTI_HYPER:
                if USE_PREDICTOR:
                    output, xp1_, xp2, xq1_res, xq1, hyper_dec, xp3, xq3 = self.model(orig_image, 2)
                    xp1, _ = self.context_model(xq1_res, xq1, hyper_dec)
                else:
                    output, xp1_, xp2, xq1, hyper_dec, xp3, xq3 = self.model(orig_image, 2)
                    xp1, _ = self.context_model(xq1, hyper_dec)
            else:
                if lambda_rd:
                    output, xp1, xp2, xq1, x3 = self.model(orig_image, 2, lambda_rd)
                else:
                    output, xp1, xp2, xq1, x3 = self.model(orig_image, 2)
            output.clamp_(0, 1)
            out_criterion = self.get_loss(output, xp1, xp2, xp3, anchor_image)

        if inital_loss > out_criterion['overall_loss'].item():
            orig_image.requires_grad = False
            returned_image = orig_image
        else:
            returned_image = orig_image_copy

        improvement = (out_criterion['overall_loss'].item() - inital_loss)/inital_loss
        print('improvement is: ' + str(improvement))
        
        if USE_MULTI_HYPER:
            del output, xp1_, xp2, xq1, hyper_dec, xp3, xq3, xp1, out_criterion, orig_image_copy
            if USE_PREDICTOR:
                del xq1_res
        else:
            del output, xp1, xp2, xq1, x3, xp3, out_criterion, orig_image_copy

        return returned_image

